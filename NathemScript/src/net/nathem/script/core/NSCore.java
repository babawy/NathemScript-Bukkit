package net.nathem.script.core;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import net.nathem.script.editor.Editor;
import net.nathem.script.editor.EditorCommand;
import net.nathem.script.enums.LogType;


public class NSCore extends JavaPlugin{

	public Editor Editor;
	private DevListener DevListener;
	private NathemListener NathemListener;
	public ArrayList<NathemWorld> registeredWorlds;

	@Override
	public void onDisable() {

		super.onDisable();
	}


	@Override
	public void onEnable() {
		
		super.onEnable();
		this.registeredWorlds = new ArrayList<NathemWorld>();
		
		this.Editor = new Editor(this);
		this.DevListener = new DevListener(this);
		this.NathemListener = new NathemListener(this);
		
		// LISTENERS
		this.getServer().getPluginManager().registerEvents(this.Editor.getListener(), this);
		this.getServer().getPluginManager().registerEvents(this.DevListener, this);
		this.getServer().getPluginManager().registerEvents(this.NathemListener, this);
		
		NSCore.log("Nathem Script Core loaded");
		
		
	}



	public static void log(String message, LogType logType)
	{
		switch(logType)
		{
			case CONSOLE: case LOG:
				ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
				console.sendMessage(ChatColor.YELLOW+"NS> " + ChatColor.WHITE+message);
				break;
			//case LOG:
				//System.out.println(new SimpleDateFormat("dd/MM/yyyy H:m").format(new Date()) + " | " + message);
				//break;
			case CHAT:
				Bukkit.broadcastMessage("[NS]" + message);
				break;
			
		}
	}
	
	public static void log(String message)
	{
		NSCore.log(message, LogType.CONSOLE);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		switch(cmd.getName())
		{
		case "nse":
			new EditorCommand(this.Editor, sender, args);
			break;
		
		}
		return false;
		
	}
	
	public NathemWorld getNathemWorld(World world)
	{
		for(NathemWorld nw : this.registeredWorlds)
		{
			if(nw.getWorld().equals(world)) return nw;
		}
		return null;
	}

}
